import {environment} from '../../environments/environment';

export const AUTH = `${environment.api}/auth`;
export const CONTACTS = `${environment.api}/contacts`;
export const USERS = `${environment.api}/users`;
export const TRANSACTIONS = `${environment.api}/transactions`;

export const AUTH_ENDPOINTS = {
  SIGN_IN: `${environment.api}/auth`,
  SIGN_UP: `${environment.api}/users`
};
