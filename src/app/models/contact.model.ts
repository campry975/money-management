import {Transaction} from './transaction.model';

export class Contact {
    constructor(
        public _id: string,
        public firstName: string,
        public lastName: string,
        public transactions: Transaction[],
        public backgroundColor: string,
        public phone?: string
    ) {}
}
