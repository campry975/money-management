import {Contact} from './contact.model';

export class Transaction {
    constructor(
        public contacts: Contact[] = [],
        public debtors: {[key: string]: {complete: boolean, value: number}} = {},
        public value: number = 0,
        public includesUser = false,
        public active = true,
        public notes?: string,
        public _id?: string,
        public createdAt: Date = new Date(),
        public updatedAt?: Date,
    ) {}
}
