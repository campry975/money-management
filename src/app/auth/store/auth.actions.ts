import {Action} from '@ngrx/store';
import {User} from '../user.model';

export const SIGN_IN = '[Auth] Sign In';
export const AUTO_SIGN_IN = '[Auth] Sign In on App Launch';
export const SIGN_UP = '[Auth] Sign Up';
export const AUTH_SUCCESS = '[Auth] Successful login/sign up';
export const AUTH_FAIL = '[Auth] Set error on failed auth';
export const CLEAR_ERROR = '[Auth] Clear auth error';
export const LOG_OUT = '[Auth] Log Out';

export class SignIn implements Action {
  readonly type = SIGN_IN;

  constructor(public payload: { email: string, password: string }) {
  }
}

export class AutoSignIn implements Action {
  readonly type = AUTO_SIGN_IN;
}

export class SignUp implements Action {
  readonly type = SIGN_UP;

  constructor(public payload: { firstName: string, lastName: string, email: string, password: string }) {
  }
}

export class AuthSuccess implements Action {
  readonly type = AUTH_SUCCESS;

  constructor(public payload: User) {
  }
}

export class AuthFail implements Action {
  readonly type = AUTH_FAIL;

  constructor(public payload: string) {
  }
}

export class ClearError implements Action {
  readonly type = CLEAR_ERROR;
}

export class LogOut implements Action {
  readonly type = LOG_OUT;
}

export type AuthActions =
  AuthSuccess |
  AuthFail |
  LogOut |
  SignIn |
  SignUp |
  AutoSignIn |
  ClearError;

