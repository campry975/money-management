import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import * as AuthActions from './auth.actions';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {of, from, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {AUTH_ERRORS} from '../auth-errors';
import {User} from '../user.model';
import {Storage} from '@ionic/storage';
import {fromPromise} from 'rxjs/internal-compatibility';
import {AUTH, USERS} from '../../shared/endpoints';

@Injectable()
export class AuthEffects {

  @Effect()
  public authSignUp$ = this.actions$.pipe(
    ofType(AuthActions.SIGN_UP),
    switchMap(authData => this.signUp(authData).pipe(
      map((authResponse: User) => this.setUser(authResponse)),
      catchError(err => this.catchErr(err))
    ))
  );

  @Effect()
  public authSignIn$ = this.actions$.pipe(
    ofType(AuthActions.SIGN_IN),
    switchMap(authData => this.signIn(authData).pipe(
      map(authResponse => this.setUser(authResponse)),
      catchError(err => this.catchErr(err))
    )),
  );

  @Effect()
  public autoSignIn$ = this.actions$.pipe(
    ofType(AuthActions.AUTO_SIGN_IN),
    switchMap(() => this.getUserFromStorage()),
    map((userData: string) => userData ? this.checkToken(JSON.parse(userData)) : new AuthActions.LogOut()),
  );

  @Effect({dispatch: false})
  public authRedirect$ = this.actions$.pipe(
    ofType(AuthActions.AUTH_SUCCESS),
    tap(() => this.router.navigate(['./home/dashboard']))
  );

  @Effect({dispatch: false})
  public authLogOut = this.actions$.pipe(
    ofType(AuthActions.LOG_OUT),
    tap(() => this.router.navigate(['./auth']))
  );

  constructor(private actions$: Actions,
              private http: HttpClient,
              private router: Router,
              private storage: Storage) {
  }

  signIn(authData: AuthActions.SignIn) {
    return this.http.post<any>(AUTH, authData.payload);
  }

  signUp(authData: AuthActions.SignUp) {
    return this.http.post<any>(USERS, authData.payload);
  }

  setUser(user: User) {
    // return fromPromise(this.storage.set('userData', JSON.stringify(user)));
    this.storage.set('userData', JSON.stringify(user));
    return new AuthActions.AuthSuccess(user);
  }

  getUserFromStorage(): any {
    return fromPromise(this.storage.get('userData'));
  }

  checkToken(user) {
    if (user.token) {
      return new AuthActions.AuthSuccess(user);
    } else {
      return new AuthActions.LogOut();
    }
  }

  catchErr(error: HttpErrorResponse) {
    console.log(error);
    if (!error.error) {
      return of(new AuthActions.AuthFail('An unexpected error occurred'));
    }
    const err = error.error;
    const errMessage = AUTH_ERRORS.hasOwnProperty(err) ? AUTH_ERRORS[err] : 'An unexpected error occurred';
    return of(new AuthActions.AuthFail(errMessage));
  }
}
