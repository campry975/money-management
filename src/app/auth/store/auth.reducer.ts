import {User} from '../user.model';
import * as AuthActions from './auth.actions';

export interface State {
  user: User;
  authError: any;
  loading: boolean;
}

const initialState: State = {
  user: null,
  authError: null,
  loading: false
};

export function authReducer(state = initialState, action: AuthActions.AuthActions) {
  switch (action.type) {
    case AuthActions.AUTH_SUCCESS:
      return {
        ...state,
        user: action.payload,
        authError: null,
        loading: false
      };
    case AuthActions.AUTH_FAIL:
      return {
        ...state,
        authError: action.payload,
        loading: false
      };
    case AuthActions.SIGN_IN:
      return {
        ...state,
        authError: null,
        loading: true
      };
    case AuthActions.LOG_OUT:
      return {
        ...state,
        user: null,
        authError: null,
        loading: false
      };
    case AuthActions.CLEAR_ERROR:
      return {
        ...state,
        authError: null
      };
    default:
      return state;
  }
}
