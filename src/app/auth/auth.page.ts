import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {AlertController} from '@ionic/angular';
import {Observable, Subject} from 'rxjs';
import {Router} from '@angular/router';
import {select, Store} from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as AuthActions from './store/auth.actions';
import {MustMatch} from '../validators/must-match';
import {takeUntil} from 'rxjs/operators';
import {AlertService} from '../services/alert.service';

@Component({
  selector: 'app-contacts',
  templateUrl: 'auth.page.html',
  styleUrls: ['auth.page.scss']
})
export class AuthPage implements OnInit {

  public authForm: FormGroup;

  public isLoginMode = true;
  public isLoading = false;
  public error: string = null;
  public auth$ = new Subject();

  constructor(private router: Router,
              private store: Store<fromApp.AppState>,
              private alertService: AlertService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.authForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
    }, {
      validators: [
        MustMatch('password', 'confirmPassword')
      ]
    });

    this.disableSignUpValidators();

    this.store.pipe(
      select('auth'),
      takeUntil(this.auth$))
      .subscribe(authState => {
        this.isLoading = authState.loading;
        this.error = authState.authError;
        if (this.error) {
          this.alertService.presentAlert(this.error);
        }
      });
  }

  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
    if (this.isLoginMode) {
      this.disableSignUpValidators();
    } else {
      this.enableSignUpValidators();
    }
  }

  disableSignUpValidators() {
    this.authForm.get('firstName').disable();
    this.authForm.get('lastName').disable();
    this.authForm.get('confirmPassword').disable();
  }

  enableSignUpValidators() {
    this.authForm.get('firstName').enable();
    this.authForm.get('lastName').enable();
    this.authForm.get('confirmPassword').enable();
  }

  onSubmit(e: any) {
    e.preventDefault();
    if (this.authForm.valid) {
      if (this.isLoginMode) {
        this.store.dispatch(new AuthActions.SignIn(this.authForm.value));
      } else {
        this.store.dispatch(new AuthActions.SignUp(this.authForm.value));
      }
    }
  }

}
