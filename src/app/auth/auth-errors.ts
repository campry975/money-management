export const AUTH_ERRORS = {
    EMAIL_EXISTS: 'This email exists already',
    INVALID_EMAIL: 'Email is invalid',
    INVALID_PASSWORD: 'Password is invalid',
    EMAIL_NOT_FOUND: 'User with entered email not registered'
};
