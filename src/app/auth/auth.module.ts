import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthPage} from './auth.page';
import {RouterModule} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [
    AuthPage
  ],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: AuthPage }]),
  ],
  providers: [
  ]
})
export class AuthModule { }
