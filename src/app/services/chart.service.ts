import {ElementRef, Injectable} from '@angular/core';
import {Transaction} from '../models/transaction.model';
import {Chart} from 'chart.js';
import {Contact} from '../models/contact.model';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  private transactions: Transaction[];
  private contacts: Contact[];

  constructor() {
  }

  public createChart(transactions: Transaction[], contacts: Contact[], chartElement: ElementRef) {
    this.transactions = transactions;
    this.contacts = contacts;
    return this.initChart(chartElement);
  }

  private createChartConfig() {
    const chartConfig = {
      labels: [],
      data: [],
      backgroundColor: [],
      borderColor: []
    };

    for (const c of this.contacts) {
      chartConfig.labels.push((`${c.firstName} ${c.lastName}`));
      chartConfig.backgroundColor.push(c.backgroundColor.replace(/rgb/, 'rgba').replace(/\)/, ',0.2)'));
      chartConfig.borderColor.push(c.backgroundColor);
      let value = 0;
      for (const t of this.transactions) {
        if (t.active) {
          // @ts-ignore
          for (const [key, d] of Object.entries(JSON.parse(t.debtors))) {
            // @ts-ignore
            if (key === c._id && !d.complete) {
              // @ts-ignore
              value += +d.value;
            }
          }
        }
      }
      chartConfig.data.push(value);
    }
    for (const value of chartConfig.data) {
      if (value === 0) {
        const index = chartConfig.data.indexOf(value);
        for (const arr of Object.values(chartConfig)) {
          arr.splice(index, 1);
        }
      }
    }
    return chartConfig;
  }

  private initChart(chartElement) {
    const chart = this.createChartConfig();

    return new Chart(chartElement.nativeElement, {
      type: 'doughnut',
      data: {
        labels: chart.labels,
        datasets: [{
          label: '# of Votes',
          data: chart.data,
          backgroundColor: chart.backgroundColor,
          borderColor: chart.borderColor,
          borderWidth: 1
        }]
      },
      options: {
        maintainAspectRatio: false,
      }
    });
  }
}
