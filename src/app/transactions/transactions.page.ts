import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import * as fromApp from '../store/app.reducer';
import * as fromTransactions from './store/transactions.reducer';
import * as TransactionActions from './store/transactions.actions';
import {ModalController} from '@ionic/angular';
import {Transaction} from '../models/transaction.model';
import {select, Store} from '@ngrx/store';
import {takeUntil} from 'rxjs/operators';
import {AlertService} from '../services/alert.service';
import {TransactionModalComponent} from './transaction-modal/transaction-modal.component';

@Component({
  selector: 'app-transactions',
  templateUrl: 'transactions.page.html',
  styleUrls: ['transactions.page.scss']
})
export class TransactionsPage implements OnInit, OnDestroy {

  public modal: any;
  public transactions$ = new Subject();
  public transactionList: Transaction[];
  public error: string;

  constructor(public modalController: ModalController,
              private store: Store<fromApp.AppState>,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.store.pipe(
      select('transactions'),
      takeUntil(this.transactions$))
      .subscribe((data: fromTransactions.State) => {
        this.transactionList = data.transactions;
        this.error = data.transactionError;
        if (this.error) {
          this.alertService.presentAlert(this.error);
        }
      });
  }

  async presentModal(transaction?: Transaction) {
    if (transaction) {
      this.store.dispatch(new TransactionActions.StartEditTransaction(transaction));
    }
    this.modal = await this.modalController.create({
      component: TransactionModalComponent
    });
    return await this.modal.present();
  }

  public deleteTransaction(id: string) {
    this.store.dispatch(new TransactionActions.StartDeleteTransaction(id));
  }

  ngOnDestroy(): void {
    this.transactions$.next();
  }

}
