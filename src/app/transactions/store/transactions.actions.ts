import {Action} from '@ngrx/store';
import {Transaction} from '../../models/transaction.model';
import {Contact} from '../../models/contact.model';

export const GET_TRANSACTIONS = '[Transactions] Get Transactions';
export const SET_TRANSACTIONS = '[Transactions] Set Transactions to app state';

export const ADD_TRANSACTION = '[Transactions] Create Transaction';
export const SET_TRANSACTION = '[Transactions] Add Transaction to app state';

export const START_DELETE_TRANSACTION = '[Transactions] Delete Transaction';
export const DELETE_TRANSACTION = '[Transactions] Remove Transaction from state';

export const START_UPDATE_TRANSACTION = '[Transactions] Update Transaction';
export const UPDATE_TRANSACTION = '[Transactions] Update Transaction in app state';

export const START_EDIT_TRANSACTION = '[Transactions] Set transaction in state to edit';
export const STOP_EDIT_TRANSACTION = '[Transactions] Clear editing transaction in state';

export const SET_ERROR = '[Transaction] Set error';


export class AddTransaction implements Action {
  readonly type = ADD_TRANSACTION;

  constructor(public payload: {
    contacts: string[],
    debtors: {[key: string]: {complete: boolean, value: number}} | any,
    active: boolean,
    notes: string,
    value: number,
    includesUser: boolean
  }) {
  }
}

export class SetTransaction implements Action {
  readonly type = SET_TRANSACTION;

  constructor(public payload: Transaction) {
  }
}

export class GetTransactions implements Action {
  readonly type = GET_TRANSACTIONS;
}

export class SetTransactions implements Action {
  readonly type = SET_TRANSACTIONS;

  constructor(public payload: Transaction[]) {
  }
}

export class StartDeleteTransaction implements Action {
  readonly type = START_DELETE_TRANSACTION;

  constructor(public payload: string) {
  }
}

export class DeleteTransaction implements Action {
  readonly type = DELETE_TRANSACTION;

  constructor(public payload: string) {
  }
}

export class StartUpdateTransaction implements Action {
  readonly type = START_UPDATE_TRANSACTION;

  constructor(public payload: {
    _id: string,
    contacts: string[],
    debtors: {[key: string]: {complete: boolean, value: number}} | any,
    active: boolean,
    notes: string,
    value: number,
    includesUser: boolean
  }) {
  }
}

export class UpdateTransaction implements Action {
  readonly type = UPDATE_TRANSACTION;

  constructor(public payload: Transaction) {
  }
}

export class StartEditTransaction implements Action {
  readonly type = START_EDIT_TRANSACTION;

  constructor(public payload: Transaction) {
  }
}

export class StopEditTransaction implements Action {
  readonly type = STOP_EDIT_TRANSACTION;
}

export class SetError implements Action {
  readonly type = SET_ERROR;

  constructor(public payload: string) {
  }
}

export type TransactionsActions =
  AddTransaction |
  SetTransactions |
  GetTransactions |
  SetTransaction |
  StartDeleteTransaction |
  DeleteTransaction |
  StartUpdateTransaction |
  UpdateTransaction |
  StartEditTransaction |
  StopEditTransaction |
  SetError;
