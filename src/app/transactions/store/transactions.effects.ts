import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import * as TransactionsActions from './transactions.actions';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Transaction} from '../../models/transaction.model';
import {TRANSACTIONS} from '../../shared/endpoints';
import {of} from 'rxjs';

@Injectable()
export class TransactionsEffects {

  @Effect()
  public getTransactions$ = this.actions$.pipe(
    ofType(TransactionsActions.GET_TRANSACTIONS),
    switchMap(() => this.http.get(TRANSACTIONS)),
    map((data: Transaction[]) => new TransactionsActions.SetTransactions(data))
  );

  @Effect()
  public addTransaction$ = this.actions$.pipe(
    ofType(TransactionsActions.ADD_TRANSACTION),
    switchMap((data: TransactionsActions.AddTransaction) => this.http.post(TRANSACTIONS, data.payload).pipe(
      map((transaction: Transaction) => new TransactionsActions.SetTransaction(transaction))
    )),
  );

  @Effect()
  public updateTransaction$ = this.actions$.pipe(
    ofType(TransactionsActions.START_UPDATE_TRANSACTION),
    map((data: TransactionsActions.StartUpdateTransaction) => data.payload),
    switchMap(transaction => this.http.put(`${TRANSACTIONS}/${transaction._id}`, transaction).pipe(
      // @ts-ignore
      // tap((updatedTransaction: Transaction) => updatedTransaction.debtors = JSON.parse(updatedTransaction.debtors)),
      map((updatedTransaction: Transaction) => {
        // console.log(updatedTransaction);
        return new TransactionsActions.UpdateTransaction(updatedTransaction)
      }),
      catchError(err => this.catchErr(err))
    ))
  );

  @Effect()
  public deleteTransaction$ = this.actions$.pipe(
    ofType(TransactionsActions.START_DELETE_TRANSACTION),
    map((data: TransactionsActions.DeleteTransaction) => data.payload),
    switchMap((id: string) => this.http.delete(`${TRANSACTIONS}/${id}`).pipe(
      map(() => new TransactionsActions.DeleteTransaction(id)),
      catchError(err => this.catchErr(err))
    )),
  );

  constructor(private actions$: Actions,
              private http: HttpClient) {
  }

  catchErr(error: HttpErrorResponse) {
    console.log(error);
    if (!error.error) {
      return of(new TransactionsActions.SetError('An unexpected error occurred'));
    }
    const err = error.error;
    return of(new TransactionsActions.SetError(err));
  }
}
