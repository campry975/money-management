import * as TransactionsActions from './transactions.actions';
import {Contact} from '../../models/contact.model';
import {Transaction} from '../../models/transaction.model';

export interface State {
  transactions: Transaction[];
  transactionError: string;
  transactionEdit: Transaction;
}

const initialState: State = {
  transactions: [],
  transactionError: null,
  transactionEdit: null
};

export function transactionsReducer(state: State = initialState, action: TransactionsActions.TransactionsActions) {
  switch (action.type) {
    case TransactionsActions.SET_TRANSACTION:
      return {
        ...state,
        transactions: [...state.transactions, action.payload],
        transactionError: null
      };
    case TransactionsActions.SET_TRANSACTIONS:
      return {
        ...state,
        transactions: action.payload,
        transactionError: null
      };
    case TransactionsActions.DELETE_TRANSACTION:
      return {
        ...state,
        transactions: state.transactions.filter(t => t._id !== action.payload),
        transactionError: null
      };
    case TransactionsActions.UPDATE_TRANSACTION:
      return {
        ...state,
        transactions: state.transactions.map(transaction => {
          if (transaction._id === action.payload._id) return action.payload;
          return transaction;
        }),
        transactionError: null,
        transactionEdit: null
      };
    case TransactionsActions.START_EDIT_TRANSACTION:
      return {
        ...state,
        transactionEdit: {...action.payload}
      };
    case TransactionsActions.STOP_EDIT_TRANSACTION:
      return {
        ...state,
        transactionEdit: null
      };
    case TransactionsActions.SET_ERROR:
      return {
        ...state,
        transactionError: action.payload
      };
    default:
      return state;
  }
}
