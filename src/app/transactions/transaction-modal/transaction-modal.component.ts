import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {select, Store} from '@ngrx/store';
import * as fromApp from '../../store/app.reducer';
import {map, take} from 'rxjs/operators';
import {Transaction} from '../../models/transaction.model';
import * as TransactionActions from '../store/transactions.actions.js';
import {TransactionService} from '../transaction.service';
import {Contact} from '../../models/contact.model';

@Component({
  selector: 'app-view-transaction-modal',
  templateUrl: './transaction-modal.component.html',
  styleUrls: ['./transaction-modal.component.scss'],
})
export class TransactionModalComponent implements OnInit {

  public isEditMode = false;

  public transaction: Transaction;
  public isEditDebtors = false;
  public isLoading = true;

  constructor(private modalCtrl: ModalController,
              private transactionService: TransactionService,
              private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {
    this.store
      .pipe(
        select('transactions'),
        take(1),
        map(state => state.transactionEdit),
      ).subscribe(transaction => {
      if (transaction) {
        this.transaction = transaction;
        // @ts-ignore
        this.transaction.debtors = JSON.parse(this.transaction.debtors);
        this.isEditMode = true;
      } else {
        this.transaction = new Transaction();
        this.isEditDebtors = true;
      }
      this.transactionService.init(this.transaction);
      this.isLoading = false;
    });
  }

  save() {
    this.transactionService.checkBeforeSave(() => {
      const {_id, value, notes, contacts, debtors, includesUser} = this.transaction;
      const payload = {
        _id,
        value,
        notes,
        contacts: contacts.map(c => c._id),
        debtors,
        includesUser,
        active: this.transactionService.isActiveDebtors()
      };
      if (this.isEditMode) {
        this.store.dispatch(new TransactionActions.StartUpdateTransaction(payload));
      } else {
        delete payload._id;
        this.store.dispatch(new TransactionActions.AddTransaction(payload));
      }
      this.close();
    });
  }

  toggleEditDebtors() {
    this.isEditDebtors = !this.isEditDebtors;
  }

  async close() {
    await this.modalCtrl.dismiss({
      dismissed: true
    });
    this.store.dispatch(new TransactionActions.StopEditTransaction());
  }

  getDebtorsTotalValue(): number {
    return this.transactionService.getDebtorsTotalValue();
  }

  addNewContact(contact: Contact) {
    this.transactionService.addNewContact(contact);
  }

  deleteContact(contact: Contact) {
    this.transactionService.deleteContact(contact);
  }

  setAverageValue() {
    this.transactionService.setAverageValue();
  }

}
