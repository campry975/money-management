import {AfterViewInit, Component, ElementRef, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Contact} from '../../models/contact.model';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {fromEvent, Subject, Subscription} from 'rxjs';
import {debounceTime, map, take, takeUntil} from 'rxjs/operators';
import * as fromApp from '../../store/app.reducer';
import * as fromContacts from '../../contacts/store/contacts.reducer';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-transaction-user-input',
  templateUrl: './transaction-user-input.component.html',
  styleUrls: ['./transaction-user-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TransactionUserInputComponent),
      multi: true
    }
  ]
})
export class TransactionUserInputComponent implements OnInit, ControlValueAccessor, AfterViewInit, OnDestroy {

  @Output() public contactAdded = new EventEmitter();
  @Output() public contactDeleted = new EventEmitter();

  @Input() selectedContacts: Contact[] = [];
  public contactList: Contact[];
  public contactListFiltered: Contact[];

  @ViewChild('input', {static: false, read: ElementRef}) input: ElementRef;
  public inputChanges$: Subscription;
  private destroy$ = new Subject();

  constructor(private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {
    this.store.select('contacts').pipe(
      map((state: fromContacts.State) => state.contacts),
      take(1),
    ).subscribe(contacts => {
      this.contactList = contacts;
    });
  }

  ngAfterViewInit(): void {
    this.inputChanges$ = fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        takeUntil(this.destroy$),
        debounceTime(300),
        map((event: any) => event.target.value)
      )
      .subscribe(keyword => {
        if (!keyword.length) {
          this.contactListFiltered = null;
        } else {
          this.contactListFiltered = this.contactList.filter(contact => {
            const regex = new RegExp(keyword, 'gi');
            return contact.firstName.match(regex) || contact.lastName.match(regex);
          });
          console.log('contact list', this.contactList);
          console.log('contactListFiltered', this.contactListFiltered);
        }
      });
  }

  addContact(contact: Contact) {
    this.selectedContacts.push(contact);
    this.input.nativeElement.value = '';
    this.contactListFiltered = [];
    this.contactAdded.emit(contact);
    this.propagateChange(this.selectedContacts);
  }

  removeContact(id: string) {
    this.selectedContacts = this.selectedContacts.filter(c => c._id === id ? this.contactDeleted.emit(c) : c);
    this.propagateChange(this.selectedContacts);
  }

  propagateChange = (_: any) => {
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {
  }

  writeValue(value: any) {
    if (value) {
      this.selectedContacts = value;
    }
    console.log('write', this.selectedContacts);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

}
