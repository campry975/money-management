import {Injectable} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {AlertService} from '../services/alert.service';
import {Transaction} from '../models/transaction.model';
import {Contact} from '../models/contact.model';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  public transaction: Transaction;

  constructor(private modalCtrl: ModalController,
              private alertService: AlertService) {
  }

  public init(transaction: Transaction) {
    this.transaction = transaction;
  }

  addNewContact(contact: Contact) {
    this.transaction.debtors[contact._id] = {
      complete: false,
      value: 0
    };
  }

  deleteContact(contact: Contact) {
    if (this.transaction.debtors.hasOwnProperty(contact._id)) {
      delete this.transaction.debtors[contact._id];
    }
  }

  checkBeforeSave(cb: () => void) {
    const diff = this.getDebtorsDifference();
    if (diff !== 'EQUAL') {
      let alertButtons;

      if (diff === 'BIGGER' && !this.transaction.includesUser) {
        alertButtons = [{
          text: 'Increase debtors values',
          cssClass: 'primary',
          handler: () => {
            this.addDiffToDebtors();
          }
        }, {
          text: 'Decrease transaction value',
          cssClass: 'primary',
          handler: () => {
            this.transaction.value = this.getDebtorsTotalValue();
          }
        }, {
          text: 'Rest is mine',
          cssClass: 'primary',
          handler: () => {
            this.transaction.includesUser = true;
          }
        }, {
          text: 'Cancel',
          cssClass: 'primary',
          role: 'cancel'
        }
        ];
        this.alertService.presentAlert(
          `Total of all debts (${this.getDebtorsTotalValue()}) are smaller then transaction value (${this.transaction.value})`,
          'Warning',
          alertButtons
        );
      } else if (diff === 'SMALLER') {
        alertButtons = [{
          text: 'Increase transaction value',
          cssClass: 'primary',
          handler: () => {
            this.transaction.value = this.getDebtorsTotalValue();
          }
        }, {
          text: 'Cancel',
          cssClass: 'primary',
          role: 'cancel'
        }
        ];
        this.alertService.presentAlert(
          `Total of all debts are bigger (${this.getDebtorsTotalValue()}) then transaction value (${this.transaction.value})`,
          'Warning',
          alertButtons);
      } else {
        cb();
      }
    } else {
      if (diff === 'EQUAL' && this.transaction.includesUser) {
        this.transaction.includesUser = false;
      }
      cb();
    }
  }

  getDebtorsTotalValue() {
    return Object.values(this.transaction.debtors).reduce((acc, debtor) => acc + debtor.value, 0);
  }

  getDebtorsDifference() {
    const debtValue = this.getDebtorsTotalValue();
    return this.transaction.value - debtValue > 5 ? 'BIGGER' : debtValue - this.transaction.value > 5 ? 'SMALLER' : 'EQUAL';
  }

  calculateAverageValue() {
    return Math.round(this.transaction.value / (this.transaction.contacts.length + (this.transaction.includesUser ? 1 : 0)));
  }

  addDiffToDebtors() {
    const diff = this.transaction.value - this.getDebtorsTotalValue();
    const debtors = Object.values(this.transaction.debtors);
    const singleValue = Math.round(diff / debtors.length);
    if (diff > 0) {
      debtors.forEach(d => {
        d.value += singleValue;
        d.complete = false;
      });
    }
  }

  setAverageValue(id?: string) {
    const debtors = this.transaction.debtors;
    const average = this.calculateAverageValue();
    if (id) {
      debtors[id].value = average;
    } else {
      Object.values(debtors).forEach((d: any) => {
        if (d.value < average) {
          d.complete = false;
        }
        d.value = average;
      });
    }
  }

  isActiveDebtors(): boolean {
    let active = false;
    for (const d of Object.values(this.transaction.debtors)) {
      if (!d.complete) active = true; break;
    }
    return active;
  }
}
