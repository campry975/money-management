import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TransactionsPage} from './transactions.page';
import {TransactionUserInputComponent} from './transaction-user-input/transaction-user-input.component';
import {TransactionModalComponent} from './transaction-modal/transaction-modal.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{path: '', component: TransactionsPage}])
  ],
  declarations: [
    TransactionsPage,
    TransactionModalComponent,
    TransactionUserInputComponent
  ],
  entryComponents: [
    TransactionModalComponent
  ]
})
export class TransactionsPageModule {
}
