import {Component, OnInit} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {select, Store} from '@ngrx/store';
import {AppState} from './store/app.reducer';
import * as fromAuth from './auth/store/auth.reducer';
import * as AuthActions from './auth/store/auth.actions';
import * as ContactsActions from './contacts/store/contacts.actions';
import * as TransactionActions from './transactions/store/transactions.actions';
import {map, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {User} from './auth/user.model';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  private userReady$ = new Subject();

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private store: Store<AppState>
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    this.store.dispatch(new AuthActions.AutoSignIn());
    this.store
      .pipe(
        select('auth'),
        takeUntil(this.userReady$),
        map((auth: fromAuth.State) => auth.user)
      )
      .subscribe((user: User) => {
        if (user) {
          this.store.dispatch(new ContactsActions.GetContacts());
          this.store.dispatch(new TransactionActions.GetTransactions());
          this.userReady$.next();
        }
      });
  }
}
