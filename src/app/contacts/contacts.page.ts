import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {select, Store} from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as ContactsActions from './store/contacts.actions';
import * as fromContacts from './store/contacts.reducer';
import {ModalController} from '@ionic/angular';
import {ContactsModalComponent} from './contacts-modal/contacts-modal.component';
import {Contact} from '../models/contact.model';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AlertService} from '../services/alert.service';

@Component({
  selector: 'app-contacts',
  templateUrl: 'contacts.page.html',
  styleUrls: ['contacts.page.scss']
})
export class ContactsPage implements OnInit {

  public modal: any;
  public error: string = null;
  public contacts$ = new Subject();
  public contactsList: Contact[];

  constructor(private http: HttpClient,
              private store: Store<fromApp.AppState>,
              public modalController: ModalController,
              private alertService: AlertService) {
    this.store.dispatch(new ContactsActions.GetContacts());
  }

  ngOnInit() {
    this.store.pipe(
      select('contacts'),
      takeUntil(this.contacts$))
      .subscribe(authState => {
        this.contactsList = authState.contacts;
        this.error = authState.contactError;
        if (this.error) {
          this.alertService.presentAlert(this.error);
        }
      });
  }

  async presentModal(contact?: Contact) {
    this.modal = await this.modalController.create({
      component: ContactsModalComponent,
      componentProps: {
        formData: contact ? contact : null
      }
    });
    return await this.modal.present();
  }

  public deleteContact(id: string) {
    this.store.dispatch(new ContactsActions.StartDeleteContact(id));
  }

}
