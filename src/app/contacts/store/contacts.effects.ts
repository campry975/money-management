import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import * as ContactsActions from './contacts.actions';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Contact} from '../../models/contact.model';
import {CONTACTS} from '../../shared/endpoints';
import {of} from 'rxjs';

@Injectable()
export class ContactsEffects {

  @Effect()
  public getContacts$ = this.actions$.pipe(
    ofType(ContactsActions.GET_CONTACTS),
    switchMap(() => this.http.get(CONTACTS).pipe(
      map((data: Contact[]) => new ContactsActions.SetContacts(data)),
      catchError(err => this.catchErr(err))
    ))
  );

  @Effect()
  public addContact$ = this.actions$.pipe(
    ofType(ContactsActions.ADD_CONTACT),
    switchMap((data: ContactsActions.AddContact) => this.http.post(CONTACTS, data.payload).pipe(
      map((contact: Contact) => new ContactsActions.SetContact(contact)),
      catchError(err => this.catchErr(err))
    )),
  );

  @Effect()
  public updateContact$ = this.actions$.pipe(
    ofType(ContactsActions.START_UPDATE_CONTACT),
    switchMap((data: ContactsActions.StartUpdateContact) => this.http.put(`${CONTACTS}/${data.payload._id}`, data.payload).pipe(
      map(() => new ContactsActions.UpdateContact(data.payload)),
      catchError(err => this.catchErr(err))
    ))
  );

  @Effect()
  public deleteContact$ = this.actions$.pipe(
    ofType(ContactsActions.START_DELETE_CONTACT),
    tap((data) => console.log(data)),
    map((data: ContactsActions.StartDeleteContact) => data.payload),
    switchMap((id: string) => this.http.delete(`${CONTACTS}/${id}`).pipe(
      map(() => new ContactsActions.DeleteContact(id)),
      catchError(err => this.catchErr(err))
    )),
  );

  constructor(private actions$: Actions,
              private http: HttpClient) {
  }

  catchErr(error: HttpErrorResponse) {
    console.log(error);
    if (!error.error) {
      return of(new ContactsActions.SetError('An unexpected error occurred'));
    }
    const err = error.error;
    return of(new ContactsActions.SetError(err));
  }
}
