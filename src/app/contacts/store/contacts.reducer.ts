import * as ContactsActions from './contacts.actions';
import {Contact} from '../../models/contact.model';

export interface State {
  contacts: Contact[];
  editedContact: Contact;
  contactError: string;
}

const initialState: State = {
  contacts: [],
  editedContact: null,
  contactError: null
};

export function contactsReducer(state: State = initialState, action: ContactsActions.ContactsActions) {
  switch (action.type) {
    case ContactsActions.SET_CONTACT:
      return {
        ...state,
        contacts: [...state.contacts, action.payload],
        contactError: null
      };
    case ContactsActions.SET_CONTACTS:
      return {
        ...state,
        contacts: action.payload,
        contactError: null
      };
    case ContactsActions.UPDATE_CONTACT:
      return {
        ...state,
        contacts: state.contacts.map((contact: Contact) => {
          if (contact._id === action.payload._id) return action.payload;
          return contact;
        }),
        contactError: null
      };
    case ContactsActions.DELETE_CONTACT:
      console.log('reducer', action.type, action.payload);
      return {
        ...state,
        contacts: state.contacts.filter(c => c._id !== action.payload),
        contactError: null
      };
    case ContactsActions.SET_ERROR:
      return {
        ...state,
        contactError: action.payload
      };
    default:
      return state;
  }
}
