import {Action} from '@ngrx/store';
import {Contact} from '../../models/contact.model';

export const GET_CONTACTS = '[Contacts] Get Contacts';
export const SET_CONTACTS = '[Contacts] Storing Contacts Locally';

export const ADD_CONTACT = '[Contacts] Create Contact';
export const SET_CONTACT = '[Contacts] Set Contact';

export const START_DELETE_CONTACT = '[Contacts] Delete Contact';
export const DELETE_CONTACT = '[Contacts] Remove Contact from state';

export const START_UPDATE_CONTACT = '[Contacts] Update Contact';
export const UPDATE_CONTACT = '[Contacts] Update Contact in state';

export const SET_ERROR = '[Contact] Set contact error';

export class AddContact implements Action {
  readonly type = ADD_CONTACT;

  constructor(public payload: { firstName: string, lastName: string, phone?: string }) {
  }
}

export class SetContact implements Action {
  readonly type = SET_CONTACT;

  constructor(public payload: Contact) {
  }
}

export class GetContacts implements Action {
  readonly type = GET_CONTACTS;
}

export class SetContacts implements Action {
  readonly type = SET_CONTACTS;

  constructor(public payload: Contact[]) {
  }
}

export class StartUpdateContact implements Action {
  readonly type = START_UPDATE_CONTACT;

  constructor(public payload: Contact) {
  }
}

export class UpdateContact implements Action {
  readonly type = UPDATE_CONTACT;

  constructor(public payload: Contact) {
  }
}

export class StartDeleteContact implements Action {
  readonly type = START_DELETE_CONTACT;

  constructor(public payload: string) {
  }
}

export class DeleteContact implements Action {
  readonly type = DELETE_CONTACT;

  constructor(public payload: string) {
  }
}

export class SetError implements Action {
  readonly type = SET_ERROR;

  constructor(public payload: string) {
  }
}


export type ContactsActions =
  AddContact |
  SetContacts |
  GetContacts |
  SetContact |
  StartDeleteContact |
  DeleteContact |
  SetError |
  StartUpdateContact |
  UpdateContact;
