import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ContactsPage } from './contacts.page';
import {ContactsModalComponent} from './contacts-modal/contacts-modal.component';
import {ColorPickerModule} from 'ngx-color-picker';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: ContactsPage }]),
    ColorPickerModule
  ],
  declarations: [ContactsPage, ContactsModalComponent],
  entryComponents: [ContactsModalComponent],
})
export class ContactsPageModule {}
