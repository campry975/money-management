import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as ContactsActions from '../store/contacts.actions';
import * as fromApp from '../../store/app.reducer';
import {Store} from '@ngrx/store';
import {Contact} from '../../models/contact.model';

@Component({
  selector: 'app-contacts-modal',
  templateUrl: './contacts-modal.component.html',
  styleUrls: ['./contacts-modal.component.scss'],
})
export class ContactsModalComponent implements OnInit {
  @Input() formData: Contact;
  public editMode = false;
  public contactForm: FormGroup;
  public backgroundColor: string;

  constructor(private modalCtrl: ModalController,
              private fb: FormBuilder,
              private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {
    this.contactForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['']
    });

    if (this.formData) {
      this.editMode = true;
      this.contactForm.patchValue(this.formData);
      this.backgroundColor = this.formData.backgroundColor;
    }
  }

  public formSubmit(e) {
    e.preventDefault();
    if (this.contactForm.valid) {
      if (this.editMode) {
        const updatedContact = {...this.contactForm.value, _id: this.formData._id, backgroundColor: this.backgroundColor};
        this.store.dispatch(new ContactsActions.StartUpdateContact(updatedContact));
      } else {
        const createdContact = {...this.contactForm.value, backgroundColor: this.backgroundColor};
        this.store.dispatch(new ContactsActions.AddContact(createdContact));
      }
      this.close();
    }
  }

  public async close() {
    await this.modalCtrl.dismiss({
      dismissed: true
    });
  }

}
