import * as fromContacts from '../contacts/store/contacts.reducer';
import * as fromAuth from '../auth/store/auth.reducer';
import * as fromTransactions from '../transactions/store/transactions.reducer';
import {ActionReducerMap} from '@ngrx/store';

export interface AppState {
  contacts: fromContacts.State;
  auth: fromAuth.State;
  transactions: fromTransactions.State;
}

export const appReducer: ActionReducerMap<AppState> = {
  contacts: fromContacts.contactsReducer,
  auth: fromAuth.authReducer,
  transactions: fromTransactions.transactionsReducer
};
