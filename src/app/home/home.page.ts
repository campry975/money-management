import {Component, OnInit, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import {ChartService} from '../services/chart.service';
import * as Chart from 'chart.js';
import {combineLatest, Observable, Subject, Subscription} from 'rxjs';
import {map, take, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements AfterViewInit {

  public chart: Chart;
  private ngUnsubscribe: Subject<void> = new Subject();

  @ViewChild('chartContainer', {static: false, read: ElementRef}) chartContainer: ElementRef;
  @ViewChild('chartCanvas', {static: false, read: ElementRef}) chartCanvas: ElementRef;

  constructor(private store: Store<fromApp.AppState>,
              private chartService: ChartService) {
  }

  ionViewDidEnter() {
    combineLatest(
      this.store.pipe(select('transactions')),
      this.store.pipe(select('contacts'))
    ).pipe(
      takeUntil(this.ngUnsubscribe),
      map(([transactions, contacts]) => [transactions.transactions, contacts.contacts])
    ).subscribe(([transactions, contacts]) => {
      if (transactions.length && contacts.length) {
        this.chart = this.chartService.createChart(transactions, contacts, this.chartCanvas);
      }
    });
  }

  ionViewDidLeave() {
    this.ngUnsubscribe.next();
  }

  ngAfterViewInit(): void {
  }

}
