export const environment = {
    production: false,
    api: 'https://still-sea-21770.herokuapp.com/api'
    // api: 'http://localhost:5002/api'
};
